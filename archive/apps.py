from django.apps import AppConfig


class ArchiveConfig(AppConfig):
    name = 'archive'
    verbose_name = 'RSVP Archive'

    def ready(self):
        from . import signals
