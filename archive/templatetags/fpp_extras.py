from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter(name='keylist')
@stringfilter
def keylist(value, arg):
    '''Replace all values of arg from the given string with ", "'''
    return value.replace(arg, ', </dd><dd>')

@register.filter(name='deslugify')
@stringfilter
def deslugify(value):
    '''Convert slugs back to space separated strings.'''
    return value.replace('-', ' ')
