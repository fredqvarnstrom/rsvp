from os import path
import pickle
from pprint import pprint

from django.db.models.signals import post_save
from django.db.models.signals import post_delete
from django.dispatch import receiver

from rsvp.settings import MEDIA_ROOT
from .models import Document
from .whoosh_search import Index
from .ocrPdf import Converter


@receiver(post_save, sender=Document)
def add_item_to_index_queue(sender, **kwargs):
    '''Save added/edited Django database items so that they can be added to
    the whoosh index later.'''
    index_queue_file = MEDIA_ROOT + 'temp/index_queue'
    if path.exists(index_queue_file):
        with open(index_queue_file, 'rb') as temp:
            index_queue = pickle.load(temp)
    else:
        index_queue = []

    document = kwargs['instance']
    index_queue.append(document.id)

    with open(index_queue_file, 'wb') as temp:
        pickle.dump(index_queue, temp, pickle.HIGHEST_PROTOCOL)


@receiver(post_delete, sender=Document)
def add_item_to_remove_queue(sender, **kwargs):
    '''Removing Django database items so that they can be added to
    the whoosh index later.'''
    index_queue_file = MEDIA_ROOT + 'temp/remove_queue'
    if path.exists(index_queue_file):
        with open(index_queue_file, 'rb') as temp:
            index_queue = pickle.load(temp)
    else:
        index_queue = []

    document = kwargs['instance']
    index_queue.append(document.id)

    with open(index_queue_file, 'wb') as temp:
        pickle.dump(index_queue, temp, pickle.HIGHEST_PROTOCOL)
