var pkg = require('./package.json'),
    gulp = require('gulp'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    minify = require('gulp-minify-css'),
    concat = require('gulp-concat');


gulp.task('css', function() {
  return gulp.src(['./css/normalize.css', './css/slicknav.css', './css/style.css'])
  .pipe(autoprefixer({
          browsers: ['> 5%'],
          cascade: false
        }))
  .pipe(concat('style.min.css'))
  .pipe(minify({compatibility: 'ie8'}))
  .pipe(gulp.dest('archive/static/fpp_archive/dist/css'));
});

gulp.task('watch', function() {
  gulp.watch('./css/*.css', ['css']);
});

gulp.task('default', ['css']);
