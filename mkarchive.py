#!/usr/bin/env python3
'''Create a temporary index from the existing FPP datastore.'''

import os
from pathlib import Path

from archive.whoosh_search import Index

def main():
    '''Main loop.'''
    index = Index('indexdir')
    txt_path = Path('txt')
    txt_list = list(txt_path.glob('**/*.txt'))
    doc_id = 100000000

    index.create()

    for d in os.listdir('/home/will/Code/temp/staging/rsvp/archive_txt'):
        p = Path('/home/will/Code/temp/staging/rsvp/archive_txt/' + d)
        doc_id = ((doc_id // 100000000) * 100000000) + 100000000

        for f in p.iterdir():
            # Metadata defaults
            author = None
            publication = None
            pub_date = None
            title = None

            url = os.path.join('/media/documents/',
                               p.name.lower().replace(' ', '_').replace(',', ''),
                               f.stem + '.pdf')
            metadata = f.stem.lower().split(sep='_')

            if len(metadata) != 4:
                print(metadata)
                continue
            else:
                title = metadata[0]
                author = metadata[1]
                pub_date = metadata[2]
                publication = metadata[3]

            with f.open() as fh:
#                keyword = ''
                content = fh.read()
#                for word in KEYWORD_LIST:
#                    if word in content:
#                        keyword += word + ','

            index.add(doc_id=str(doc_id),
                      title=title,
                      url=url,
                      content=content,
                      author=author,
                      publication=publication,
                      pub_date=pub_date,
#                      keyword=keyword[:-1],
                      category=p.name.replace(',', ''))

#            print(doc_id, title, url, author, publication, pub_date, keyword[:-1], p.name)
#            print(doc_id, title, url, author, publication, pub_date, p.name)

            doc_id += 1

if __name__ == '__main__':
    main()
