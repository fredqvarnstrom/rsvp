from django.shortcuts import get_object_or_404, render

from .models import Article


def index(request):
    article_list = Article.objects.order_by('id')
    context = {'article_list': article_list}
    return render(request, 'blog/articles.html', context)


def article(request, article_id):
    article = get_object_or_404(Article, pk=article_id)
    return render(request, 'blog/article.html', {'article': article})
