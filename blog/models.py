from django.db import models

class Article(models.Model):
    title = models.CharField(max_length=300)
    content = models.TextField()
    banner = models.ImageField(upload_to='banners')
    thumbnail = models.ImageField(upload_to='article_thumbs', blank=True, null=True)

    def __str__(self):
        return self.title
