# RSVP-America (https://rsvpamerica.org/)
================================================

**
Don't judge me by the content of this site!
I just developed this site on demand of a client.
**
## Install dependencies
    
-  Install `MySQL` on the server
        
        sudo apt-get install libmysqlclient-dev
        sudo apt-get install mysql-server

- Install some packages
    
        sudo apt-get install libxml2-dev libxslt-dev

- Create database, user and grant access
        
        mysql> CREATE DATABASE rsvp_america;
        mysql> CREATE USER 'rsvp_america'@'localhost' IDENTIFIED BY '8rvWjpVjfKu3JYRk6iK5excdcflHb7';
        mysql> GRANT ALL ON rsvp_america.* TO 'rsvp_america'@'localhost';
        mysql> FLUSH PRIVILEGES;

-  Set environment value to indicate source directory
    
        export RSVP_PATH="/home/ubuntu/work/rsvp"
  
-  Install dependencies
  
        pip install -r requirements.txt

## Execute Django server
    
- HTTP server:

        python manage.py runserver

- HTTPS server:
    
        python manage.py runsslserver --certificate cert/rsvpamerica.org.crt --key cert/rsvpamerica.org.key
        
## Execute live server with nginx+gunicorn
        
        cd ~/rsvp
        python manage.py collectstatic --noinput
        gunicorn rsvp.wsgi -b 127.0.0.1:8001 --daemon -w1 --log-file=out.log
        
        sudo apt-get install nginx
        sudo cp nginx-http.conf /etc/nginx/sites-available/rsvp
        sudo ln -s /etc/nginx/sites-available/rsvp /etc/nginx/sites-enable/rsvp
        sudo service nginx restart